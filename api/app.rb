require 'sinatra'
require 'sinatra-websocket'
require 'json'

set :server, 'thin'
set :port, 8000
set :bind, '0.0.0.0'
set :sockets, []

# всем клиентам рассылаем новое количество подключений
def udpateCounter
  count = settings.sockets.length
  msg = JSON.generate( { client_count: count })
  EM.next_tick { settings.sockets.each{|s| s.send(msg) } }
end

get '/' do
  if !request.websocket?
    erb :index
  else
    request.websocket do |ws|
      ws.onopen do
        msg = { message: "Hello World!", time: Time.now.getutc.to_i.to_s, type: :system }
        ws.send(JSON.generate msg)
        settings.sockets << ws
        udpateCounter
      end
      ws.onmessage do |msg|
        EM.next_tick { settings.sockets.each{|s| s.send(msg) } }
      end
      ws.onclose do
        warn("websocket closed")
        settings.sockets.delete(ws)
        udpateCounter
      end
    end
  end
end

__END__
@@ index
<html>
  <body>
     <h1>Simple Echo & Chat Server</h1>
     <div>
        VM name: <%=ENV['VM_NAME']%>
        Clients: <span id="client_count">0</span>
     </div>
     <form id="form">
       <input type="text" id="input" value="send a message"></input>
     </form>
     <div id="msgs"></div>
  </body>

  <style type="text/css">
    .message {
      display: flex;
      justify-content: space-between;
      margin: 10 20;
    }
    .system {
      border: none;
      border-bottom: 1px solid #ffaaaa;
    }
    .user {
      border: none;
      border-bottom: 1px solid #aaaaff;
    }
  </style>

  <script type="text/javascript">
    window.onload = function(){
      (function(){
        const showMessage = function(el){
          return function(msg) {
            const {
              message,
              type = 'system',
              time = (Date.now()/1000).toFixed(),
              } = msg;
            const element = `<div class="${type} message">${message} <span class="time">${(new Date(time*1000)).toUTCString()}</span></div>`
            el.innerHTML = `${element}${el.innerHTML}`;
          }
        }(document.getElementById('msgs'));

        const updateCounter = function(el) {
          return (count = 0) => { el.innerText = count };
        }(document.getElementById('client_count'));


        const ws       = new WebSocket('ws://' + window.location.host + window.location.pathname);
        ws.onopen    = ()   => { showMessage({ message: 'websocket opened'}); };
        ws.onclose   = ()   => { showMessage({ message: 'websocket closed'}); }
        ws.onmessage = (m)  => {
          const msg = JSON.parse(m.data);
          if(!!msg.message) {
            showMessage(msg);
          } else {
            updateCounter(msg.client_count);
          }
        };

        const sender = function(f){
          const input     = document.getElementById('input');
          input.onclick = function(){ input.value = "" };
          f.onsubmit    = function(){
            if(!!input.value) {
              const msg = { message: input.value, time: (Date.now()/1000).toFixed(), type: 'user' };
              ws.send(JSON.stringify(msg));
              input.value = "";
            }
            return false;
          }
        }(document.getElementById('form'));
      })();
    }
  </script>
</html>
